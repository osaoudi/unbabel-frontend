import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LaunchIcon from '@material-ui/icons/Launch';
import LanguageIcon from '@material-ui/icons/Language';         
import Chip from '@material-ui/core/Chip';
import SnackbarContent from '@material-ui/core/SnackbarContent';


class ListComponent extends React.Component {
  
  constructor(props) {
  
	super(props);
	}
  
  state = {
    requests: [],
	hasError:false   
  };
   
 // Fires when an error is detected
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });  
  }

  // Condition for comparing object
  compare(a,b) {
  
	if(a.translatedText != null && b.translatedText != null )
		{if (a.translatedText.length < b.translatedText.length)
			return -1;
		if (a.translatedText.length > b.translatedText.length)
			return 1;
		}
	return 0;
	}
	
// function  to sort list by message length
	sortState(a,b){
	
		var objs = b;
		objs.sort(a);
		this.setState({ requests: objs });
	}

// Function to update shown data
  componentWillReceiveProps(nextProps) {
 
	this.sortState(this.compare,nextProps.demands );
    this.setState({ requests: nextProps.demands });
  
};

// Fires when list element is clicked
	handlClick(i) {
	
		var obj = this.state.requests;
		if(obj[i].flag ===0 )
		{
			obj[i].flag = 1;
			obj[i].classe = 'animated flipInX delay-2s';
		}
		else
		{
			obj[i].flag = 0;
			obj[i].classe = '';
		}	
		this.setState({ requests: obj });
};

    render() {
	
	const greenStyle ={
			backgroundColor:'#2ecc71'
	}
	const yellowStyle ={
			backgroundColor:'#f1c40f'
	}
	
    return (
		
	 <div >
		<List component="nav">
		{ 
		this.state.requests.map((request,index)=>{
			return (<ListItem button>
				{request.flag===1?
				<ListItemIcon>
					<LanguageIcon />
				</ListItemIcon>:
				<ListItemIcon>
					<LaunchIcon />
				</ListItemIcon>}
          <ListItemText inset data-id={index} className={request.classe} onClick={this.handlClick.bind(this,index)} primary={request.flag===0?request.text:request.translatedText} />
			  <Chip style={request.status==='completed'?greenStyle:yellowStyle}  label={request.status==='new'?'requested':request.status==='translating'?'pending':'translated'} />
		  </ListItem>	
		);
        })
		}
		</List>
	  {this.state.hasError===true?
		<SnackbarContent  message="An error has occured"  />:null
		}
    </div>	
	     );
           }
           }


           export default ListComponent;