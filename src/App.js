import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import List from './components/listComponent/ListChip.js'
import Paper from '@material-ui/core/Paper';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import './App.css';

const endpoint = 'http://localhost:5000';

class App extends Component {
  state = {
    demands: [],
	keyword:'',
	hasError:false
  };

// adds keyword to list  and sends api /translate request
  addToList()
  {
  
  try{
	var obj={};
	var self = this;
	fetch(endpoint+'/translate', {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
				},
	body: JSON.stringify({
		keyword:this.state.keyword
	})
	}).then((resp) => { return resp.json();}) // Transform the data into json
  .then(function(data) {
		obj.text = data.text;
		obj.status = data.status;
		obj.uid = data.uid;
		obj.price = data.price;
		var newArr = self.state.demands;
		newArr.push(obj);
		self.setState({demands:newArr});
});
 }
 catch(e)
	{console.log(e);}
  }
  
    
updateKeyword(evt) 
{
   this.setState({keyword: evt.target.value});
};


// Fires when an error is detected
componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
}
  
  
// Starts the timer to check if translation is completed
 componentDidMount() {
    this.countdown = setInterval(this.timer.bind(this), 2000);
  };

// Stops the timer to prevent memory leak
  componentWillUnmount() {
    clearInterval(this.countdown);
  };

// Function to check the translation status
  timer() {
	try{
	 var obj={};
	 var self = this;
	this.state.demands.map((request, i)=>{
		if(request.status !=="completed")
		{
			fetch(endpoint+'/check', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
					},
			body: JSON.stringify({
				uid:request.uid
			})
		}).then((resp) => {	
				return resp.json();}) // Transform the data into json
		.then(function(data) {
				obj = self.state.demands;
				request.status = data.status;
				request.translatedText = data.translatedText;
				request.flag=0;
				request.classe='';
				obj[i] = request;
				self.setState({
				demands: obj
						});
					});
		}
 });
	}
	catch(e)
	{
		console.log(e);
	}
 }; 
  
  render() {
  const listStyle={marginTop:'5px'};
  const buttonStyle={margin:'5px'};

    return (
      <div className="App">
       <Paper>   
        <div className="container">
		 <h3 className="text-center  pt-5">Unbabel Translate</h3>
            <div id="" className="row justify-content-center align-items-center">
                <div id="" className="col-md-6">
                    <div id="" className="col-md-12">			
					<TextField id="full-width" InputLabelProps={{shrink: true,}} placeholder="Type a word to translate" value={this.state.keyword} onChange={this.updateKeyword.bind(this)}  fullWidth margin="normal"/>	
					<Button variant="contained" size="small" color="primary" style={buttonStyle} onClick={this.addToList.bind(this)}>
					Translate
					</Button>
                    </div>
                </div>
            </div>
        </div>
    </Paper>
	<div id="" className="offset-md-4 col-md-4" style={listStyle}>
	<Paper>		   
		<List demands={this.state.demands}></List>
    </Paper>
	</div>
	
{this.state.hasError===true?
		<SnackbarContent  message="An error has occured"  />:null
		}
	
      </div>
	  
    );
  }
}

export default App;
